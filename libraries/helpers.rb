module LampNodeCommon
  module Helpers
    #
    # Returns a list of node names that are DB servers for the cluster
    #
    def db_node_names
      get_cluster_data['db']['nodes']
    end

    #
    # Returns a list of nodes that are DB servers for the cluster
    #
    def db_nodes
      db_node_names.map do |n|
        search(:node, "name:#{n}").first
      end.compact
    end

    #
    # Returns the name of the master of the DB cluster
    #
    def cluster_db_master_name
      get_cluster_data['db']['master']
    end

    #
    # Returns the master node of the DB cluster
    #
    def cluster_db_master
      if master_name = cluster_db_master_name
        search(:node, "name:#{master_name}").first
      else
        Chef::Log.warn "cluster_db_master: master not found"
        nil
      end
    end

    #
    # Returns the IP address of the DB cluster, i.e. of the current master
    #
    def cluster_db_ipaddress
      if master = cluster_db_master
        Chef::Log.info "cluster_db_ipaddress master=#{master.inspect}"
        node_cluster_ip(master)
      else
        Chef::Log.warn "cluster_db_ipaddress master not found"
        nil
      end
    end

    def node_cluster_ip(n)
      addresses = n['network']['interfaces']['eth1']['addresses']
      addresses.detect { |addr,data| data['family'] == 'inet' }[0]
    end

  protected
    def get_cluster_data
      node['lamp-cluster-config']
    end
  end
end

Chef::Recipe.send(:include, LampNodeCommon::Helpers)
